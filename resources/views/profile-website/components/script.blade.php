<!-- Javascript -->
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
        
<!-- Slider -->
<script src="{{  asset('assets/js/tiny-slider.js') }}"></script>
<script src="{{  asset('assets/js/tiny-slider-init.js') }}"></script>

<!-- MK Lightbox -->
<script src="{{  asset('assets/js/mklb.js') }}"></script>

<!-- Contact -->
<script src="{{  asset('assets/js/contact.js') }}"></script>

<!-- Counter init -->
<script src="{{  asset('assets/js/counter.init.js') }}"></script>

<!-- Feather icon -->
<script src="{{  asset('assets/js/feather.min.js') }}"></script>

<!-- Typed -->
<script src="{{  asset('assets/js/typed.js') }}"></script>
<script src="{{  asset('assets/js/typed.init.js') }}"></script>

<!-- Switcher -->
<script src="{{  asset('assets/js/switcher.js') }}"></script>

<!-- Main Js -->
<script src="{{  asset('assets/js/app.js') }}"></script>