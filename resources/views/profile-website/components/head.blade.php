<!-- favicon -->
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">
<!-- Bootstrap -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- MK Lightbox -->
<link href="{{ asset('assets/css/mklb.css') }}" rel="stylesheet" type="text/css"/>
<!-- Icons -->
<link href="{{ asset('assets/css/materialdesignicons.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- Slider -->               
<link href="{{ asset('assets/css/tiny-slider.css') }}" rel="stylesheet"/>
<!-- Main css File -->
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" id="theme-opt"/>
<link href="{{ asset('assets/css/colors/default.css') }}" rel="stylesheet" id="color-opt"/>
{{-- Custom CSS --}}
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">