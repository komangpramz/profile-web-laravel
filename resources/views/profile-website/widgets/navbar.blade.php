<!-- Navbar Start -->
<nav id="navbar" class="navbar navbar-expand-lg fixed-top navbar-custom navbar-light sticky">
    <div class="container">
        <!-- Logo container-->
        <a class="logo" href="{{ route('index') }}">Pramayasa</a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" 
        aria-label="Toggle navigation">
            <i class="mdi mdi-menu"></i>
        </button><!--end button-->

        <div class="collapse navbar-collapse navigation" id="navbarCollapse">
            <ul id="navbar-navlist" class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link {{Route::is('index') ? 'active' : ''}}" href="{{ route('index') }}">Beranda</a>
                </li><!--end nav item-->
                <li class="nav-item">
                    <a class="nav-link {{Route::is('about') ? 'active' : ''}}" href="{{ route('about') }}" >Tentang</a>
                </li><!--end nav item-->
                <li class="nav-item">
                    <a class="nav-link {{Route::is('gallery') ? 'active' : ''}}" href="{{ route('gallery') }}">Galeri</a>
                </li><!--end nav item-->             
                <li class="nav-item">
                    <a class="nav-link {{Route::is('contact') ? 'active' : ''}}" href="{{ route('contact') }}">Kontak</a>
                </li><!--end nav item-->                
            </ul>

            <ul class="top-right text-end list-unstyled list-inline mb-0 mt-2 mt-sm-0 nav-social">
                <li class="list-inline-item me-2"><a href="javascript:void(0)"><i class="mdi mdi-facebook"></i></a></li>
                <li class="list-inline-item me-2"><a href="javascript:void(0)"><i class="mdi mdi-twitter"></i></a></li>
                <li class="list-inline-item"><a href="javascript:void(0)"><i class="mdi mdi-instagram"></i></a></li>
            </ul>
            
        </div> 
    </div><!--end container-->
</nav><!--end navbar-->
<!-- Navbar End -->