 <!-- Footer Start -->
<footer class="footer footer-bar bg-black">
    <div class="container text-foot">
        <div class="row">
            <div class="col-md-6 col-12">
                <p class="mb-0 text-white-50">© <script>document.write(new Date().getFullYear())</script> Komang Pramayasa. Universitas Pendidikan Ganesha.</p>
            </div>
            <div class="col-md-6 col-12">
                <ul class="list-unstyled mb-0 social-icon text-right">
                    <li class="list-inline-item me-1"><a href="javascript:void(0)" class="rounded-circle"><i class="mdi mdi-facebook"></i></a></li>
                    <li class="list-inline-item me-1"><a href="javascript:void(0)" class="rounded-circle"><i class="mdi mdi-twitter"></i></a></li>
                    <li class="list-inline-item me-1"><a href="javascript:void(0)" class="rounded-circle"><i class="mdi mdi-instagram"></i></a></li>                    
                </ul>
            </div>
        </div>        
    </div><!--end container-->
</footer><!--end footer-->
<!-- Footer End -->