<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pram Profile Website = @yield('title')</title>

    @include('profile-website.components.head')
</head>
<body data-bs-spy="scroll" data-bs-target="#navbar-navlist" data-bs-offset="20">

    {{-- Navbar WIdget --}}
    @include('profile-website.widgets.navbar')

    {{-- Main Content --}}
    @yield('content')

    {{-- Footer Widget --}}
    @include('profile-website.widgets.footer')

    <a href="javascript: void(0);" class="back-to-top btn btn-icon btn-soft-primary" id="back-to-top" onclick="topFunction()">
        <i data-feather="arrow-up" class="icons"></i>
    </a>
    <!-- Back to top -->

    {{-- Script Component --}}
    @include('profile-website.components.script')
</body>
</html>