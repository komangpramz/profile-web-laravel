@extends('profile-website.app')
@section('title', 'Tentang Saya')
@section('content')
    {{-- About Section --}}
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-12 ph-3 mt-5">
                    <img src="{{ asset('assets/images/about/profile.jpeg') }}" alt="Profile Photo" class="about-image img-fluid">
                </div>
                <div class="col-md-8 col-12">
                    <div class="about-text">
                        <h3>Tentang Saya</h3>
                        <hr class="line">
                        <p class="text-light-muted mb-2"> 
                            Hallo Semua! Perkenalkan nama saya Komang Pramayasa merupakan anak ketiga dari empat bersaudara. Saya lahir di Denpasar pada tanggal 22 Juli 2000. Orang-orang sering beranggapan bahwa saya adalah orang yang ceria dan humoris. Namun, tidak menutup kemungkinan juga bagi saya untuk tertutup dengan orang-orang tertentu, terutama untuk orang-orang yang baru saya kenal. 
                        </p>
                        <p class="text-light-muted mb-2"> 
                            Saat ini, saya menempuh studi di Universitas Pendidikan Ganesha Program Studi Sistem Informasi. Saya sangat tertarik dalam bidang pengembangan dan juga desain aplikasi. Hal ini saya buktikan dengan terus mempelajari hal-hal baru dalam dunia teknologi. Ketertarikan saya dalam dunia teknologi dilatarbelakangi oleh hobi saya dalam bermain video game, saya melihat bahwa video game merupakan hal yang sangat keren pada masa itu, dan saya sangat tertarik untuk dapat mempelajarinya. Hingga akhirnya, disinilah saya, seorang mahasiswa Sistem Informasi yang masih terus belajar untuk sebuah cita-cita dan harapan! Doakan saya ya! 
                        </p>
                        <p class="text-light-muted mb-2">
                            Selain tertarik dalam bidang teknologi, saya juga tertarik dalam kegiatan sosial kemanusiaan. Bagi saya, membantu sesama manusia merupakan hal yang paling mulia. Walaupun dengan kekurangan yang saya miliki, saya selalu berusaha untuk tetap berbagi kebaikan kepada sesama manusia. Hal ini saya buktikan dengan mengikuti UKM KSR-PMI UNIT UNDIKSHA dan Mahasiswa Siaga Bencana. 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- End About Section --}}

    <!-- Start Section -->
    <section class="section skill-ed bg-light">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="container-title text-center mb-4 pb-2">
                        <div class="titles">
                            <h2 class="title text-capitalize mb-4">Pendidikan dan Penghargaan</h2>
                            <p class="pera-title para-desc-600 text-light-muted mb-0 mx-auto">Berikut adalah beberapa jenjang pendidikan yang pernah saya tempuh serta penghargaan yang pernah saya raih</p>
                            <span></span>
                        </div>
                    </div>                    
                </div>                
            </div>
            <div class="row">
                {{-- Education --}}
                <div class="col-6">
                    <div class="row">
                        <div class="main-timeline education">
                            <div class="timeline mt-4">
                                <a href="javascript:void(0)" class="timeline-content">
                                    <div class="timeline-icon text-primary">
                                        <i data-feather="briefcase" class="fea icon-md"></i>
                                    </div>
                                    <h3 class="title mb-0">SD N 8 Padangsambian</h3>
                                    <span class="badge skill-badge education-badge badge-light">2007 - 2013</span>
                                    <p class="description text-light-muted mt-2">
                                        Saya memulai pendidikan di SD Negeri 8 Padangsambian selama 6 tahun masa pendidikan.
                                    </p>
                                </a>
                            </div>
                            <div class="timeline mt-4">
                                <a href="javascript:void(0)" class="timeline-content">
                                    <div class="timeline-icon text-primary">
                                        <i data-feather="briefcase" class="fea icon-md"></i>
                                    </div>
                                    <h3 class="title mb-0">SMP N 5 Denpasar</h3>
                                    <span class="badge skill-badge education-badge margin-50 badge-light">2013 - 2016</span>
                                    <p class="description text-light-muted mt-2">
                                        Setelah itu, saya melanjutkan pendidikan di salah satu sekolah menengah pertama terbaik di Denpasar, yakni SMP N 5 Denpasar.
                                    </p>
                                </a>
                            </div>
                            <div class="timeline mt-4">
                                <a href="javascript:void(0)" class="timeline-content">
                                    <div class="timeline-icon text-primary">
                                        <i data-feather="briefcase" class="fea icon-md"></i>
                                    </div>
                                    <h3 class="title mb-0">SMK N 1 Denpasar</h3>
                                    <span class="badge skill-badge education-badge badge-light">2016 - 2019</span>
                                    <p class="description text-light-muted mt-2">
                                        Setelah melewati jenjang pendidikan SMP, saya memutuskan untuk melanjutkan ke jenjang pendidikan SMK, tepatnya di SMK Negeri 1 Denpasar dengan program keahlian Rekayasa Perangkat Lunak. 
                                    </p>
                                </a>
                            </div>                                                        
                        </div>
                    </div>
                </div>

                {{-- Achivement --}}
                <div class="col-6">
                    <div class="row">
                        <div class="main-timeline">
                            <div class="timeline mt-4">
                                <a href="javascript:void(0)" class="timeline-content">
                                    <div class="timeline-icon text-primary">
                                        <i data-feather="award" class="fea icon-md"></i>
                                    </div>
                                    <h3 class="title mb-0">Lomba Mading Digital HMJ TI Undiksha</h3>
                                    <small class="company">Juara 3</small>
                                    <span class="badge skill-badge education-badge badge-light">2019</span>
                                    <p class="description text-light-muted mt-2">
                                        Lomba ini diselenggarakan oleh HMJ TI Undiksha pada tahun 2019, saya dengan beberapa tim berhasil meraih Juara 3 dalam pembuatan Mading Digital HMJ TI Undiksha. 
                                    </p>
                                </a>
                            </div>
                            <div class="timeline mt-4">
                                <a href="javascript:void(0)" class="timeline-content">
                                    <div class="timeline-icon text-primary">
                                        <i data-feather="award" class="fea icon-md"></i>
                                    </div>
                                    <h3 class="title mb-0">Lomba PKM Pagelaran Akhir Tahun BEM FTK Undiksha</h3>
                                    <small class="company">Juara 2</small>
                                    <span class="badge skill-badge education-badge margin-50 badge-light">2020</span>
                                    <p class="description text-light-muted mt-2">
                                        Program Kreativitas Mahasiswa atau PKM adalah salah satu kegiatan mahasiswa dalam menunagkan sebuah ide atau gagasan yang bermanfaat. Pada kegiatan ini saya bersama dengan tim berhasil meraih Juara 2 Lomba PKM PAT, khususnya dalam kategori PKM Kewirausahaan. 
                                    </p>
                                </a>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end container -->
    </section><!-- end section -->

    <!-- Skill & CTA START -->
    <section class="cta-full skill-area">
        <div class="container ">
            <div class="row position-relative">              
                <div class="col-12">
                    <div class="cta-full-img-box">
                        <div class="row justify-content-center">
                            <div class="col-12 text-center mb-3">
                                <div class="container-title">
                                    <div class="titles">
                                        <h4 class="title title-line text-capitalize mb-4">Kemampuan dan Pengetahuan<h4>
                                    </div>
                                    <p class="text-light-muted mx-auto width-max mb-0">Berikut adalah beberapa kompetensi yang saya miliki sebagai pengembang aplikasi (application developer).</p>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->

                        <div class="row align-items-center">
                            <div class="col-lg-3 col-6 mt-4 pt-2">
                                <div class="card explore-feature border-0 text-center bg-white p-3">
                                    <div class="card-body p-0">
                                        <div class="skill-icon rounded-circle shadow-lg d-inline-block">
                                            <img src="{{ asset('assets/images/skills/bootstrap.jpg') }}" alt="" class="img-fluid mx-auto">
                                        </div>                                        
                                        <span class="badge skill-badge badge-light">80%</span>
                                        <p class="mt-3 text-light-muted">Bootstrap</p>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-6 mt-4 pt-2">
                                <div class="card explore-feature border-0 text-center bg-white p-3">
                                    <div class="card-body p-0">
                                        <div class="skill-icon rounded-circle shadow-lg d-inline-block">
                                            <img src="{{ asset('assets/images/skills/laravel.png') }}" alt="" class="img-fluid mx-auto">
                                        </div>
                                        <span class="badge skill-badge badge-light">55%</span>
                                        <p class="mt-3 text-light-muted">Laravel</p>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-6 mt-4 pt-2">
                                <div class="card explore-feature border-0 text-center bg-white p-3">
                                    <div class="card-body p-0">
                                        <div class="skill-icon rounded-circle shadow-lg d-inline-block">
                                            <img src="{{ asset('assets/images/skills/js.png') }}" alt="" class="img-fluid mx-auto">
                                        </div>
                                        <span class="badge skill-badge badge-light">68%</span>
                                        <p class="mt-3 text-light-muted">Javascript</p>
                                    </div>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-6 mt-4 pt-2">
                                <div class="card explore-feature border-0 text-center bg-white p-3">
                                    <div class="card-body p-0">
                                        <div class="skill-icon rounded-circle shadow-lg d-inline-block">
                                            <img src="{{ asset('assets/images/skills/php.png') }}" alt="" class="img-fluid mx-auto">
                                        </div>
                                        <span class="badge skill-badge badge-light">75%</span>
                                        <p class="mt-3 text-light-muted">PHP</p>
                                    </div>
                                </div>
                            </div><!--end col-->    
                        </div><!-- end row -->                        

                    </div> <!-- end about detail -->
                </div> <!-- end col -->        
            </div><!--end row-->
        </div><!--end container fluid-->
    </section><!--end section-->   
    <!-- Skill & CTA End -->
@endsection