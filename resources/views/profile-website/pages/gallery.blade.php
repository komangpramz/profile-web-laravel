@extends('profile-website.app')
@section('title', 'Galeri')
@section('content')
    <!-- Projects/Work Start -->
    <section class="section" id="projects">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="container-title text-center mb-4 pb-2">
                        <div class="titles">
                            <h2 class="title text-capitalize mb-4">Galeri dan Portofolio</h2>
                            <p class="pera-title para-desc-600 text-light-muted mb-0 mx-auto">Berikut ini adalah beberapa galeri kehidupan Komang Pramayasa. Mau tau keseruannya? Yuk dilihat!</p>
                            <span></span>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->


            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card border-0 work-container work-modern position-relative d-block overflow-hidden rounded">
                        <div class="card-body p-0">
                            <img src="{{ asset('assets/images/gallery/1.jpeg') }}" class="img-fluid" alt="work-image">
                            <div class="overlay-work bg-dark"></div>
                            <div class="content">
                                <h5 class="mb-0"><a href="page-portfolio-detail.html" class="text-white title">Musyawarah Kerja XIII UKM KSR-PMI UNIT UNDIKSHA</a></h5>
                            </div>
                            <div class="icons text-center">
                                <a href="javascript:void(0)" data-src="{{ asset('assets/images/gallery/1.jpeg') }}"  data-gallery="myGal" class="text-primary work-icon bg-white d-inline-block rounded-pill mfp-image">
                                    <i data-feather="camera" class="fea icon-sm"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card border-0 work-container work-modern position-relative d-block overflow-hidden rounded">
                        <div class="card-body p-0">
                            <img src="{{ asset('assets/images/gallery/5.jpeg') }}" class="img-fluid" alt="work-image">
                            <div class="overlay-work bg-dark"></div>
                            <div class="content">
                                <h5 class="mb-0"><a href="page-portfolio-detail.html" class="text-white title">Satuan Tugas Siaga Covid-19</a></h5>
                            </div>
                            <div class="icons text-center">
                                <a href="javascript:void(0)" data-src="{{ asset('assets/images/gallery/5.jpeg') }}" data-gallery="myGal" class="text-primary work-icon bg-white d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm"></i></a>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card border-0 work-container work-modern position-relative d-block overflow-hidden rounded">
                        <div class="card-body p-0">
                            <img src="{{ asset('assets/images/gallery/6.jpeg') }}" class="img-fluid" alt="work-image">
                            <div class="overlay-work bg-dark"></div>
                            <div class="content">
                                <h5 class="mb-0"><a href="page-portfolio-detail.html" class="text-white title">Rapat Kerja HMJ TI Undiksha</a></h5>
                            </div>
                            <div class="icons text-center">
                                <a href="javascript:void(0)" data-src="{{ asset('assets/images/gallery/6.jpeg') }}" data-gallery="myGal" class="text-primary work-icon bg-white d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm"></i></a>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->  
                
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card border-0 work-container work-modern position-relative d-block overflow-hidden rounded">
                        <div class="card-body p-0">
                            <img src="{{ asset('assets/images/gallery/2.jpeg') }}" class="img-fluid" alt="work-image">
                            <div class="overlay-work bg-dark"></div>
                            <div class="content">
                                <h5 class="mb-0"><a href="page-portfolio-detail.html" class="text-white title">Alumni RPL Skensa 54</a></h5>
                            </div>
                            <div class="icons text-center">
                                <a href="javascript:void(0)" data-src="{{ asset('assets/images/gallery/2.jpeg') }}" data-gallery="myGal" class="text-primary work-icon bg-white d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm"></i></a>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card border-0 work-container work-modern position-relative d-block overflow-hidden rounded">
                        <div class="card-body p-0">
                            <img src="{{ asset('assets/images/gallery/3.jpeg') }}" class="img-fluid" alt="work-image">
                            <div class="overlay-work bg-dark"></div>
                            <div class="content">
                                <h5 class="mb-0"><a href="page-portfolio-detail.html" class="text-white title">PAT BEM FTK 2019</a></h5>
                            </div>
                            <div class="icons text-center">
                                <a href="javascript:void(0)" data-src="{{ asset('assets/images/gallery/3.jpeg') }}" data-gallery="myGal" class="text-primary work-icon bg-white d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm"></i></a>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="card border-0 work-container work-modern position-relative d-block overflow-hidden rounded">
                        <div class="card-body p-0">
                            <img src="{{ asset('assets/images/gallery/4.jpeg') }}" class="img-fluid" alt="work-image">
                            <div class="overlay-work bg-dark"></div>
                            <div class="content">
                                <h5 class="mb-0"><a href="page-portfolio-detail.html" class="text-white title">Akhir Kepengurusan HMJ TI 2020/2021</a></h5>
                            </div>
                            <div class="icons text-center">
                                <a href="javascript:void(0)" data-src="{{ asset('assets/images/gallery/4.jpeg') }}" data-gallery="myGal" class="text-primary work-icon bg-white d-inline-block rounded-pill mfp-image"><i data-feather="camera" class="fea icon-sm"></i></a>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->                                         
            </div><!--end row-->
        </div>
        <!-- End container -->
    </section>
    <!-- Projects End -->

    {{-- Video Section --}}
    <section class="section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="container-title text-center mb-4 pb-2">
                        <div class="titles">
                            <h2 class="title text-capitalize mb-4">Video</h2>
                            <p class="pera-title para-desc-600 text-light-muted mb-0 mx-auto">Ingin belajar? Berikut adalah beberapa video pembelajaran yang pernah dibuat oleh Komang Pramayasa. Feel free to watch!</p>
                            <span></span>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
            <div class="row">
                <div class="col-md-7">
                    <iframe width="100%" height="500" src="https://www.youtube.com/embed/sKk0LB7QttA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="col-md-5">
                    <iframe width="100%" height="250" src="https://www.youtube.com/embed/QiqE6HVkmZA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <iframe width="100%" height="250" src="https://www.youtube.com/embed/YIma_x2XvP0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
@endsection