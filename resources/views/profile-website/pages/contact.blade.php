@extends('profile-website.app')
@section('title', 'Kontak Saya')
@section('content')
<!-- Contact Start -->
<section class="section pb-0" id="contact">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="container-title text-center mb-4 pb-2">
                    <div class="titles">
                        <h2 class="title text-capitalize mb-4">Hubungi Saya</h2>
                        <p class="pera-title para-desc-600 text-light-muted mb-0 mx-auto">Jika kamu memiliki pertanyaan atau tertarik untuk menjalin kerjasama dengan saya, kamu bebas untuk menghubungi saya melalui kontak di bawah ini!</p>
                        <span></span>
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->

        <div class="row">
            <div class="col-md-4 mt-4 pt-2">
                <div class="contact-detail text-center">
                    <div class="icon">
                        <i data-feather="phone" class="fea icon-md"></i>
                    </div>
                    <div class="content mt-4">
                        <h5 class="title">Telephone</h5>
                        <p class="text-light-muted">+6283114682628</p>                        
                    </div>
                </div>
            </div>
            <!--end col-->

            <div class="col-md-4 mt-4 pt-2">
                <div class="contact-detail text-center">
                    <div class="icon">
                        <i data-feather="mail" class="fea icon-md"></i>
                    </div>
                    <div class="content mt-4">
                        <h5 class="title">Email</h5>
                        <p class="text-light-muted">pramayasa@undiksha.ac.id</p>                        
                    </div>
                </div>
            </div>
            <!--end col-->

            <div class="col-md-4 mt-4 pt-2">
                <div class="contact-detail text-center">
                    <div class="icon">
                        <i data-feather="map-pin" class="fea icon-md"></i>
                    </div>
                    <div class="content mt-4">
                        <h5 class="title">Location</h5>
                        <p class="text-light-muted">Jalan Kebo Iwa Utara III A, Denpasar Barat</p>                        
                    </div>
                </div>
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->

<section class="section pt-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="custom-form mb-sm-30">
                    <form method="post" action="#" name="myForm">
                        <p id="error-msg"></p>
                        <div id="simple-msg"></div>
                        <div class="row">
                            <div class="col-lg-6">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.7410835960436!2d115.17913921433599!3d-8.620837389986612!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd238d1c03df1fb%3A0xd77bfbbb12932110!2sPerumahan%20Tegal%20Sari!5e0!3m2!1sen!2sid!4v1632422398778!5m2!1sen!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12 col-md-6">
                                        <div class="form-group">
                                            <input name="name" id="name" type="text" class="form-control border rounded"
                                                placeholder="Nama Anda">
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-12 col-md-6">
                                        <div class="form-group">
                                            <input name="email" id="email" type="email"
                                                class="form-control border rounded" placeholder="Email Anda">
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input name="subject" id="subject" class="form-control border rounded"
                                                placeholder="Subjek Pesan">
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <textarea name="comments" id="comments" rows="4" class="form-control border rounded"
                                                placeholder="Isi Pesan"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 text-end">
                                        <input type="submit" id="submit" name="send"
                                            class="submitBnt btn btn btn-primary rounded" value="Kirim Pesan">
                                    </div>
                                </div>
                                <!--end row-->
                            </div>
                            <!--end col-->                           
                        </div>                        
                    </form>
                    <!--end form-->
                </div>
                <!--end custom-form-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
</section>
<!--end section-->
<!-- Contact End -->
@endsection
