@extends('profile-website.app')
@section('title', 'Index')
@section('content')

<div id="home">
    <!-- Hero Area -->
    <div class="hero-area position-relative bg-half-120 pb-0"
        style="background-image:url('{{ asset('assets/images/home/home-shape.png') }}')">
        <!--<div class="bg-overlay"></div>-->
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="home-image" style="text-align: center">
                        <img src="{{ asset('assets/images/home/hero1.png') }}" alt=""
                            class="img-fluid mx-auto position-relative">
                    </div>
                </div>
                <div class="col-lg-6 col-md-7 align-items-center">
                    <div class="title-heading mt-4 py--50 pb-200">
                        <h1 class="heading text-black">Hallo, Saya Komang Pramayasa</h1>
                        <div class="alert alert-transparent alert-pills shadow" role="alert">
                            <span class="content">Mahasiswa Sistem Informasi Undiksha</span>
                        </div>
                        <p class="pera-title text-light-muted">Saya merupakan mahasiswa yang sedang duduk di semester 5.
                            Semakin naik semester saya merasa semakin stress. Tetapi, saya tetap semangat! </p>                        
                    </div>
                </div>                
            </div>
        </div>
    </div>

    <section class="bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card public-profile border-0 rounded shadow" style="z-index: 1;">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-lg-3 col-md-4 text-md-start">
                                    <div class="about-details key-feature bg-light">
                                        <div class="media-body content">
                                            <h6 class="title mb-2">Informasi Personal</h6>
                                            <div class="bor-bottom mt-2 mb-2"></div>
                                            <p class="text-muted mb-2 mt-3 font-14"><a href="javascript:void(0)"
                                                    class="text-black">Nama: </a> Komang Pramayasa</p>
                                            <p class="text-muted mb-2 font-14"><a href="javascript:void(0)"
                                                    class="text-black">Email: </a> pramayasa@undiksha.ac.id</p>
                                            <p class="text-muted mb-2 font-14"><a href="javascript:void(0)"
                                                    class="text-black">Domisili: </a> Denpasar Barat</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-9 col-md-8">
                                    <div class="row align-items-end">
                                        <div class="col-md-12 text-md-start mt-4 mt-sm-0">
                                            <h4 class="heading me-2">Hai! Salam Kenal, Aku <span class="text-primary">Mang
                                                    Pram</span></h4>
                                            <p class="text-light-muted mt-2">Orang-orang biasa memanggil aku dengan
                                                sebutan Mang Pram. Namun, beberapa juga akrab memanggilku Komang. Aku
                                                merupakan seorang mahasiswa aktif semester V Program Studi Sistem
                                                Informasi Universitas Pendidikan Ganesha. Di kampus, aku sangat suka
                                                dalam berkegiatan, saat ini aku aktif dalam mengikuti organisasi
                                                kemanusiaan UKM KSR-PMI UNIT UNDIKSHA. Doakan aku ya semoga kegiatan
                                                perkuliahan disini selalu berjalan dengan lancar! </p>
                                        </div>
                                        <!--end col-->
                                    </div>
                                    <!--end row-->
                                </div>
                                <!--end col-->
                            </div>
                            <!--end row-->
                        </div>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->
        </div>
        <!--end container-->
    </section>
    <!--end section -->

    <!-- Service Start -->
    <section class="section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="container-title text-center mb-4 pb-2">
                        <div class="titles">
                            <h2 class="title text-capitalize mb-4">Apa Saja Yang Bisa Mang Pram Lakukan?</h2>
                            <p class="pera-title para-desc-600 text-light-muted mb-0 mx-auto">Berikut adalah beberapa
                                keahlian yang dimiliki oleh Mang Pram, namun Mang Pram juga masih perlu belajar banyak
                                hal dalam mengasah kemampuannya.</p>
                            <span></span>
                        </div>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->

            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="feature-widget text-center rounded">
                        <div class="services-icon text-primary mb-3">
                            <i data-feather="monitor" class="fea icon-md"></i>
                        </div>
                        <div class="services-texts">
                            <h3 class="title mb-3">Pengembangan Web</h3>
                            <p class="text-light-muted mb-4">Mang Pram mampu mengembangkan aplikasi khususnya website
                                untuk berbagai klien, baik personal maupun company.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="feature-widget text-center rounded">
                        <div class="services-icon text-primary mb-3">
                            <i data-feather="layout" class="fea icon-md"></i>
                        </div>
                        <div class="services-texts">
                            <h3 class="title mb-3">Desain dan Prototipe</h3>
                            <p class="text-light-muted mb-4">Mang Pram dapat merancang desain maupun prototipe produk
                                sesuai kebutuhan client dengan hasil yang memuaskan.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-12 mt-4 pt-2">
                    <div class="feature-widget text-center rounded">
                        <div class="services-icon text-primary mb-3">
                            <i data-feather="feather" class="fea icon-md"></i>
                        </div>
                        <div class="services-texts">
                            <h3 class="title mb-3">Copywriting</h3>
                            <p class="text-light-muted mb-4">Mang Pram juga pandai dalam menulis (copywriting), Komang
                                sudah sering menulis beberapa berita, artikel, dan iklan.</p>
                        </div>
                    </div>
                </div>
            </div> <!-- end row -->
        </div>
    </section>
    <!-- End Start -->

    <!-- Testimonial Start -->
    <section class="section bg-white">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="container-title text-center mb-4 pb-2">
                        <div class="titles">
                            <h2 class="title text-capitalize mb-4">Apa Kata Klien?</h2>
                            <p class="pera-title para-desc-600 text-light-muted mb-0 mx-auto">Kepuasaan klien merupakan
                                hal yang utama bagi Mang Pram, dimana Mang Pram selalu berusaha untuk memberikan yang
                                tebaik untuk klien. Berikut adalah beberapa feedback dari klien yang pernah bekerjasama
                                dengan Mang Pram. Disimak Yuk!</p>
                            <span></span>
                        </div>
                    </div>
                </div>
                <!--end col-->
            </div>
            <!--end row-->

            <div class="row">
                <div class="col-12">
                    <div class="client-review-slider">
                        <div class="tiny-slide">
                            <div class="client-review rounded shadow m-2">
                                <div class="align-items-center justify-content-start justify-content-sm-center">
                                    <div class="col-sm-auto mb-4 mb-sm-0">
                                        <img class="img-fluid avatar avatar-xl"
                                            src="{{ asset('assets/images/client/surf.jpg') }}" alt="">
                                    </div>
                                    <div class="col pl-lg-4 clt-section">
                                        <h5 class="clt-title mt-2 mb-2">Surf Camp Dreamsea</h5>
                                        <ul class="list-unstyled float-right mb-0">
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="pera-title text-light-muted mb-0 mt-2">"Kami sangat puas akan kinerja yang
                                        diberikan oleh Mang Pram pada pengembangan projek web profile Surf Camp
                                        Dreamsea"</p>
                                </div>
                            </div>
                        </div>
                        <div class="tiny-slide">
                            <div class="client-review rounded shadow m-2">
                                <div class="align-items-center justify-content-start justify-content-sm-center">
                                    <div class="col-sm-auto mb-4 mb-sm-0">
                                        <img class="img-fluid avatar avatar-xl"
                                            src="{{ asset('assets/images/client/rumah-ayam.jpg') }}" alt="">
                                    </div>
                                    <div class="col pl-lg-4 clt-section">
                                        <h5 class="clt-title mt-2 mb-2">Rumah Ayam Bali</h5>
                                        <ul class="list-unstyled float-right mb-0">
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="pera-title text-light-muted mb-0 mt-2">"Rumah Ayam Bali sangat
                                        merekomendasikan Mang Pram sebagai web developer karena tingkat kreativitas yang
                                        dimiliki oleh Mang Pram."</p>
                                </div>
                            </div>
                        </div>
                        <div class="tiny-slide">
                            <div class="client-review rounded shadow m-2">
                                <div class="align-items-center justify-content-start justify-content-sm-center">
                                    <div class="col-sm-auto mb-4 mb-sm-0">
                                        <img class="img-fluid avatar avatar-xl"
                                            src="{{ asset('assets/images/client/ergo.jpg') }}" alt="">
                                    </div>
                                    <div class="col pl-lg-4 clt-section">
                                        <h5 class="clt-title mt-2 mb-2">Ergo Consultant</h5>
                                        <ul class="list-unstyled float-right mb-0">
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="pera-title text-light-muted mb-0 mt-2">"Mang Pram adalah salah satunya
                                        developer hebat yang pernah kami temui dan kami ajak berkolaborasi. Kami sangat
                                        puas!"</p>
                                </div>
                            </div>
                        </div>
                        <div class="tiny-slide">
                            <div class="client-review rounded shadow m-2">
                                <div class="align-items-center justify-content-start justify-content-sm-center">
                                    <div class="col-sm-auto mb-4 mb-sm-0">
                                        <img class="img-fluid avatar avatar-xl"
                                            src="{{ asset('assets/images/client/belden.png') }}" alt="">
                                    </div>
                                    <div class="col pl-lg-4 clt-section">
                                        <h5 class="clt-title mt-2 mb-2">Belden Village Mall</h5>
                                        <ul class="list-unstyled float-right mb-0">
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="pera-title text-light-muted mb-0 mt-2">"Belden Village Mall sangat takjub
                                        atas hasil dari pekerjaan yang dilakukan oleh Mang Pram. Ia merupakan orang yang
                                        kreatif dan displin."</p>
                                </div>
                            </div>
                        </div>
                        <div class="tiny-slide">
                            <div class="client-review rounded shadow m-2">
                                <div class="align-items-center justify-content-start justify-content-sm-center">
                                    <div class="col-sm-auto mb-4 mb-sm-0">
                                        <img class="img-fluid avatar avatar-xl"
                                            src="{{ asset('assets/images/client/pohe.jpg') }}" alt="">
                                    </div>
                                    <div class="col pl-lg-4 clt-section">
                                        <h5 class="clt-title mt-2 mb-2">Potato Head</h5>
                                        <ul class="list-unstyled float-right mb-0">
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                            <li class="list-inline-item text-warning"><i class="mdi mdi-star"></i></li>
                                        </ul>
                                    </div>
                                    <p class="pera-title text-light-muted mb-0 mt-2">"Mang Pram merupakan orang yang
                                        kreatif dan berperan penting dalam memberikan ide menarik produk Potato Head."
                                    </p>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="hero-btn text-center mt-4 pt-2">
                        <a href="{{ route('about') }}" class="btn btn-outline-primary rounded mb-2">Kenali Saya Lebih Jauh!</a>
                    </div>
                </div>
            </div>
        </div>
        <!--end container -->
    </section>
    <!--end section -->
</div>
@endsection
