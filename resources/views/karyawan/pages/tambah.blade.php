@extends('karyawan.app')
@section('title', 'Tambah Data')
@section('content')
<div class="container">
    <div class="row p-5">
        <div class="col-md-6 offset-md-3">
            <h3 class="text-center mb-4">
                Formulir Tambah Data Karyawan
            </h3>
            <div class="forms">
                <form method="POST" action="/karyawan/store">
                    @csrf
                    <div class="mb-3">
                      <label for="nomor" class="form-label">Nomor Karyawan</label>
                      <input type="text" class="form-control" id="nomor" name="nomor" maxlength="15">                      
                    </div>
                    <div class="mb-3">
                      <label for="nama" class="form-label">Nama Karyawan</label>
                      <input type="text" class="form-control" id="nama" name="nama">
                    </div>    
                    <div class="mb-3">
                      <label for="telp" class="form-label">No Telpon</label>
                      <input type="number" class="form-control" id="telp" name="telp">
                    </div>  
                    <div class="mb-3">
                      <label for="jabatan" class="form-label">Jabatan Karyawan</label>
                      <input type="text" class="form-control" id="jabatan" name="jabatan">
                    </div>  
                    <div class="mb-3">
                        <label for="divisi" class="form-label">Divisi Karyawan</label>
                        <select class="form-select" name="divisi">
                            <option selected disabled>Pilih Divisi</option>
                            <option value="Marketing">Marketing</option>
                            <option value="Finance">Finance</option>
                            <option value="Logistic">Logistic</option>
                            <option value="Technology">Technology</option>                          
                          </select>
                      </div>                  
                    <button type="submit" class="btn btn-primary">Tambah Karyawan</button>
                  </form>
            </div>
        </div>
    </div>
@endsection