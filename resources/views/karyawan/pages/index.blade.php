@extends('karyawan.app')
@section('title', 'Index')
@section('content')    
    <div class="container">
        <div class="row p-5">
            <div class="col-md-10 offset-md-1">
                <h3 class="text-center mb-4">
                    Sistem Informasi Karyawan
                </h3>
                <a href=" {{ route('krywn.create') }} " class="btn btn-primary">Tambah Data</a>            
                <table class="table mt-1">
                    <thead>
                      <tr>                        
                        <th scope="col">No Karyawan</th>
                        <th scope="col">Nama Karyawan</th>
                        <th scope="col">No Telp</th>
                        <th scope="col">Jabatan</th>
                        <th scope="col">Divisi</th>
                        <th scope="col" class="text-center">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($karyawan as $k)                        
                      <tr>                        
                        <td>{{ $k->no_karyawan }}</td>
                        <td>{{ $k->nama_karyawan }}</td>
                        <td>{{ $k->no_telp_karyawan }}</td>
                        <td>{{ $k->jabatan_karyawan }}</td>
                        <td>{{ $k->divisi_karyawan }}</td>
                        <td class="text-center">
                            <a href="/karyawan/edit/{{$k->id}}" class="btn btn-success">Ubah</a>  
                            <a href="/karyawan/delete/{{$k->id}}" class="btn btn-danger">Hapus</a>
                        </td>
                      </tr>                         
                      @endforeach                                           
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection