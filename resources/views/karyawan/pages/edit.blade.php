@extends('karyawan.app')
@section('title', 'Edit Data')
@section('content')
<div class="container">
    <div class="row p-5">
        <div class="col-md-6 offset-md-3">
            <h3 class="text-center mb-4">
                Formulir Edit Data Karyawan
            </h3>
            <div class="forms">
                <form method="POST" action="/karyawan/update">
                    @csrf
                    @foreach ($karyawan as $k)
                    <input type="hidden" name="id" value="{{ $k->id }}">
                    <div class="mb-3">
                        <label for="nomor" class="form-label">Nomor Karyawan</label>
                        <input type="text" class="form-control" id="nomor" name="nomor" maxlength="15" value="{{ $k->no_karyawan }}">
                    </div>
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama Karyawan</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ $k->nama_karyawan }}">
                    </div>
                    <div class="mb-3">
                        <label for="telp" class="form-label">No Telpon</label>
                        <input type="number" class="form-control" id="telp" name="telp" value="{{ $k->no_telp_karyawan }}">
                    </div>
                    <div class="mb-3">
                        <label for="jabatan" class="form-label">Jabatan Karyawan</label>
                        <input type="text" class="form-control" id="jabatan" name="jabatan" value="{{ $k->jabatan_karyawan }}">
                    </div>
                    <div class="mb-3">
                        <label for="divisi" class="form-label">Divisi Karyawan</label>
                        <select class="form-select" name="divisi">
                            <option selected disabled>Pilih Divisi</option>
                            <option value="Marketing" {{$k->divisi_karyawan == 'Marketing' ? 'selected' : '' }}>Marketing</option>
                            <option value="Finance" {{$k->divisi_karyawan == 'Finance' ? 'selected' : '' }}> Finance</option>
                            <option value="Logistic" {{$k->divisi_karyawan == 'Logistic' ? 'selected' : '' }}> Logistic</option>
                            <option value="Technology" {{$k->divisi_karyawan == 'Technology' ? 'selected' : '' }}>Technology</option>
                        </select>
                    </div>
                    @endforeach
                    <button type="submit" class="btn btn-primary">Edit Data Karyawan</button>
                </form>
            </div>
        </div>
    </div>
    @endsection
