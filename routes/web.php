<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\Karyawan\KaryawanController@index');

// ROUTE WEBSITE DATA KARYAWAN
Route::prefix('karyawan')->group(function(){
    Route::get('/', 'App\Http\Controllers\Karyawan\KaryawanController@index')->name('krywn.index');    
    Route::get('/create', 'App\Http\Controllers\Karyawan\KaryawanController@create')->name('krywn.create');
    Route::post('/store', 'App\Http\Controllers\Karyawan\KaryawanController@store')->name('krywn.store');
    Route::get('/edit/{id}', 'App\Http\Controllers\Karyawan\KaryawanController@edit')->name('krywn.edit');
    Route::post('/update', 'App\Http\Controllers\Karyawan\KaryawanController@update')->name('krywn.update');


    Route::get('/delete/{id}', 'App\Http\Controllers\Karyawan\KaryawanController@delete')->name('krywn.delete');

    
});

//ROUTE PROFILE WEBSITE
Route::prefix('profile-website')->group(function(){
    Route::get('/', 'App\Http\Controllers\ProfileWebsite\HomeController@index')->name('index');    
    Route::get('about', 'App\Http\Controllers\ProfileWebsite\AboutController@index')->name('about');
    Route::get('gallery', 'App\Http\Controllers\ProfileWebsite\GalleryController@index')->name('gallery');
    Route::get('contact', 'App\Http\Controllers\ProfileWebsite\ContactController@index')->name('contact');
});
