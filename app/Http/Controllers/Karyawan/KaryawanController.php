<?php

namespace App\Http\Controllers\Karyawan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KaryawanController extends Controller
{
    public function index(){
        $karyawan = DB::table('karyawan')->orderBy('id', 'asc')->get();

        return view('karyawan.pages.index', ['karyawan' => $karyawan]);
    }

    public function create(){
        return view('karyawan.pages.tambah');
    }

    // Function menambah data karyawan
    public function store(Request $req){
        
        // Deklarasi variable inputan
        $nomor = $req->nomor;
        $nama = $req->nama;
        $telp = $req->telp;
        $jabatan = $req->jabatan;
        $divisi = $req->divisi;

        // Proses insert ke tabel karyawan
        $result = DB::table('karyawan')->insert([
            'no_karyawan' => $nomor,
            'nama_karyawan' => $nama,
            'no_telp_karyawan' => $telp,
            'jabatan_karyawan' => $jabatan,
            'divisi_karyawan' => $divisi
        ]);        

        // Check status insert data
        if($result == 1) {
            return redirect('/');
        } else {
            return 'Tambah Data Gagal';
        }
    }

    // Function untuk menampilkan halaman edit
    public function edit($id) {
        
        // Mengambil data karyawan yang ingin diedit sesuai id
        $karyawan = DB::table('karyawan')->where('id', $id)->get();

        // Parsing data karyawan ke halaman edit
        return view('karyawan.pages.edit', ['karyawan' => $karyawan]);

    }

    // Function merubah data karyawan
    public function update(Request $req){

        // Deklarasi variable inputan
        $id = $req->id;
        $nomor = $req->nomor;
        $nama = $req->nama;
        $telp = $req->telp;
        $jabatan = $req->jabatan;
        $divisi = $req->divisi;

        // Proses update data mahasiswa
        $result = DB::table('karyawan')->where('id', $id)->update([
            'no_karyawan' => $nomor,
            'nama_karyawan' => $nama,
            'no_telp_karyawan' => $telp,
            'jabatan_karyawan' => $jabatan,
            'divisi_karyawan' => $divisi
        ]);

        // Check status update data
        if($result == 1) {
            return redirect('/');
        } else {
            return 'Update Data Gagal';
        }

    }

    // Function menghapus data karyawan
    public function delete($id) {

        // Hapus data karyawan berdasarkan id
        $delete = DB::table('karyawan')->where('id', $id)->delete();

        // Check status update data
        if($delete == 1) {
            return redirect('/');
        } else {
            return 'Delete Data Gagal';
        }

    }
}
