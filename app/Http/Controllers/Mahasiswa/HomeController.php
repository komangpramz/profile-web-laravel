<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){
        $mahasiswa = DB::table('mahasiswa')->orderBy('id_mhs', 'asc')->get();

        return view('mahasiswa.pages.index', ['mahasiswa' => $mahasiswa]);
    }
}
