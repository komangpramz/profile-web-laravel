<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function index(){
        return view('mahasiswa.pages.tambah');
    }

    // Function menambah data mahasiswa
    public function store(Request $req){
        
        // Deklarasi variable inputan
        $nim = $req->nim;
        $nama = $req->nama;
        $prodi = $req->prodi;

        // Proses insert ke tabel mahasiswa
        $result = DB::table('mahasiswa')->insert([
            'nim' => $nim,
            'nama' => $nama,
            'prodi' => $prodi
        ]);        

        // Check status insert data
        if($result == 1) {
            return redirect('/');
        } else {
            return 'Tambah Data Gagal';
        }
    }

    // Function untuk menampilkan halaman edit
    public function edit($id) {
        
        // Mengambil data mahasiswa yang ingin diedit sesuai id
        $mahasiswa = DB::table('mahasiswa')->where('id_mhs', $id)->get();

        // Parsing data mahasiswa ke halaman edit
        return view('mahasiswa.pages.edit', ['mahasiswa' => $mahasiswa]);

    }

    // Function merubah data mahasiswa
    public function update(Request $req){

        // Deklarasi variable inputan
        $id = $req->id;
        $nim = $req->nim;
        $nama = $req->nama;
        $prodi = $req->prodi;

        // Proses update data mahasiswa
        $result = DB::table('mahasiswa')->where('id_mhs', $id)->update([
            'nim' => $nim,
            'nama' => $nama,
            'prodi' => $prodi
        ]);

        // Check status update data
        if($result == 1) {
            return redirect('/');
        } else {
            return 'Update Data Gagal';
        }

    }

    // Function menghapus data mahasiswa
    public function delete($id) {

        // Hapus data mahasiswa berdasarkan id
        $delete = DB::table('mahasiswa')->where('id_mhs', $id)->delete();

        // Check status update data
        if($delete == 1) {
            return redirect('/');
        } else {
            return 'Deete Data Gagal';
        }

    }
}
