<?php

namespace App\Http\Controllers\ProfileWebsite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index(){
        return view('profile-website.pages.gallery');
    }
}
