<?php

namespace App\Http\Controllers\ProfileWebsite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){
        return view('profile-website.pages.contact');
    }
}
